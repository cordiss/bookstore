﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Infrastructure;
using BookStore.DAL.Repositories.BaseRepositories.Interfaces;
using BookStore.DAL.Entities.Base;

namespace BookStore.DAL.Repositories.BaseRepositories
{
    public class BaseEFRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly StoreContext _storeContext;

        public BaseEFRepository(StoreContext storeContext)
        {
            _storeContext = storeContext;
        }

        public IEnumerable<TEntity> GetAll() 
        {

            return _storeContext.Set<TEntity>().AsNoTracking();
        }

        public PageIndexModel<TEntity> Pagination(IEnumerable<TEntity> entities, int page)
        {
            var items = entities.Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE);

            PageModel pageModel = new PageModel(entities.Count(), page, Constants.PAGE_SIZE);
            PageIndexModel<TEntity> pageIndexModel = new PageIndexModel<TEntity>
            {
                PageModel = pageModel,
                Entities = items
            };
            return pageIndexModel;
        }

        public TEntity Get(int id) 
        {
            return _storeContext.Set<TEntity>().AsNoTracking().FirstOrDefault(e => e.Id == id);
        }
        public void Put(int id, TEntity entity) 
        {
            _storeContext.Entry(entity).State = EntityState.Modified;
            _storeContext.SaveChanges();
        }
        public async Task Post(TEntity entity)
        {
            await _storeContext.Set<TEntity>().AddAsync(entity);
            await _storeContext.SaveChangesAsync();
        }
        public async Task Delete(int id)
        {
            var entity = Get(id);
            _storeContext.Set<TEntity>().Remove(entity);
            await _storeContext.SaveChangesAsync();

        }
        public async Task<bool> Exists(int id) 
        {
            var searchEntity = await _storeContext.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);

            return searchEntity == null ? false : true;
        }
    }
}