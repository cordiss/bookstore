﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Infrastructure;

namespace BookStore.DAL.Repositories.BaseRepositories.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(int id);
        PageIndexModel<TEntity> Pagination(IEnumerable<TEntity> entities, int page);
        void Put(int id, TEntity entity);
        Task Post(TEntity entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}