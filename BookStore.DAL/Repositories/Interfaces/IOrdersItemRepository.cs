﻿using System.Collections.Generic;
using BookStore.DAL.Entities;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IOrdersItemRepository
    {
        IEnumerable<OrderItem> GetAll();
        Task<OrderItem> Get(int id);
        void Put(int id, OrderItem entity);
        Task Post(OrderItem entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}