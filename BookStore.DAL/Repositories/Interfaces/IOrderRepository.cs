﻿using System.Collections.Generic;
using BookStore.DAL.Entities;
using BookStore.DAL.Infrastructure;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        PageIndexModel<Order> GetAll(string id, int page = 1, Enums.Order.Status status = Enums.Order.Status.Any, Enums.Order.Sorting sorting = Enums.Order.Sorting.CreationDataAsc);
        Task<Order> Get(int id);
        void Put(int id, Order entity);
        Task<int> Post(Order entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}