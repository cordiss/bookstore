﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Entities;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IAuthorInPrintingEdition
    {
        Task<IEnumerable<AuthorInPrintingEdition>> Get(string name);
        void Put(int id, AuthorInPrintingEdition entity);
        Task Post(AuthorInPrintingEdition entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}