﻿using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IPaymentRepository
    {
        PageIndexModel<Payment> GetAll(int page = 1);
        Payment Get(int id);
        void Put(int id, Payment entity);
        Task<int> Post(Payment entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}