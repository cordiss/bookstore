﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<string> Create(UserManager<User> _userManager, User model, string password);
        Task Delete(UserManager<User> _userManager, User model);
        PageIndexModel<User> GetUsers(string firstName, string lastName, string email, bool isActive = true, int page = 1);
        Task<User> GetUser(UserManager<User> _userManager, string id);
        Task<User> GetUserByName(UserManager<User> userManager, string userName);
        Task Edit(UserManager<User> _userManager, User model, string id);
        Task<bool> Exists(UserManager<User> _userManager, string id);
        Task<IList<string>> UserRole(UserManager<User> _userManager, User user);
        Task<bool> UserInRole(UserManager<User> _userManager, User user, Enums.User.UserRole role);
        Task SignInUser(User user, SignInManager<User> signInManager);
        Task SignOff(SignInManager<User> signInManager);
        Task<User> AuthenticateUser(string email, string password, SignInManager<User> signInManager, UserManager<User> userManager);
    }
}