﻿using System.Collections.Generic;
using BookStore.DAL.Entities;
using BookStore.DAL.Infrastructure;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IPrintingEditionRepository
    {
        PageIndexModel<PrintingEdition> GetAll(string title, string author, string type = "any", double minPrice = 0.0, double maxPrice = 99999.99, Enums.PrintingEdition.SortingMethod method = Enums.PrintingEdition.SortingMethod.CreationDataAsc, int page = 1);
        PrintingEdition Get(int id);
        void Put(int id, PrintingEdition entity);
        Task<int> Post(PrintingEdition entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}