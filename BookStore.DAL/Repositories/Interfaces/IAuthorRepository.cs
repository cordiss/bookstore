﻿using System.Collections.Generic;
using BookStore.DAL.Entities;
using BookStore.DAL.Infrastructure;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Interfaces
{
    public interface IAuthorRepository
    {
        PageIndexModel<Author> GetAll(string authorName, int page = 1);
        Author Get(int id);
        void Put(int id, Author entity);
        Task Post(Author entity);
        Task Delete(int id);
        Task<bool> Exists(int id);
    }
}