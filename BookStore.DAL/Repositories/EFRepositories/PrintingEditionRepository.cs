﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;
using BookStore.DAL.Entities;
using BookStore.DAL.Infrastructure;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class PrintingEditionRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionRepository
    {
        private readonly StoreContext _storeContext;

        public PrintingEditionRepository(StoreContext storeContext)
            :base(storeContext)
        {
            _storeContext = storeContext;
        }
        
        public PageIndexModel<PrintingEdition> GetAll(string title, string author, string type = "any", double minPrice = 0.0, double maxPrice = 99999.99, Enums.PrintingEdition.SortingMethod method = Enums.PrintingEdition.SortingMethod.CreationDataAsc, int page = 1)
        {
            IEnumerable<PrintingEdition> list; 

            if (author != null)
            {
                list = Filter(author);
            }
            else
            {
                list = base.GetAll();
            }

            Filter(ref list, title, type, minPrice, maxPrice);
            Sorting(ref list, method);

            return base.Pagination(list, page);
        }

        private void Filter(ref IEnumerable<PrintingEdition> outlist, string title, string type, double minPrice, double maxPrice)
        {
            if (title != null) outlist = outlist.Where(x => x.Title == title);
            if (type != "any") outlist = outlist.Where(x => type.Contains(x.Type.ToString()));
            outlist = outlist.Where(x => x.Price > minPrice);
            outlist = outlist.Where(x => x.Price < maxPrice);
            outlist = outlist.Where(x => x.IsRemoved != true);
        }

        private IEnumerable<PrintingEdition> Filter(string authorName)
        {
            List<PrintingEdition> list = new List<PrintingEdition>();

            var author = _storeContext.Authors.FirstOrDefault(e => e.Name == authorName);
            IEnumerable<AuthorInPrintingEdition> authorsInPE = _storeContext.AuthorInPrintingEditions.Include("PrintingEdition").Where(x => x.Author.Id == author.Id).ToList();

            foreach(AuthorInPrintingEdition d in authorsInPE)
            {
                list.Add(d.PrintingEdition);
            }
            return list;
        }

        private void Sorting(ref IEnumerable<PrintingEdition> outlist, Enums.PrintingEdition.SortingMethod method)
        {
            if(method == Enums.PrintingEdition.SortingMethod.PriceAsc) outlist = outlist.OrderBy(x => x.Price);
            if (method == Enums.PrintingEdition.SortingMethod.PriceDesc) outlist = outlist.OrderByDescending(x => x.Price);
            if (method == Enums.PrintingEdition.SortingMethod.TitleAsc) outlist = outlist.OrderBy(x => x.Title);
            if (method == Enums.PrintingEdition.SortingMethod.TitleDesc) outlist = outlist.OrderByDescending(x => x.Title);
            if (method == Enums.PrintingEdition.SortingMethod.CreationDataAsc) outlist = outlist.OrderBy(x => x.CreationData.Date);
            if (method == Enums.PrintingEdition.SortingMethod.CreationDataDesc) outlist = outlist.OrderByDescending(x => x.CreationData.Date);
        }

        public new PrintingEdition Get(int id)
        {
            return base.Get(id);
        }

        public new void Put(int id, PrintingEdition printingEdition)
        {
            base.Put(id, printingEdition);
        }

        public async new Task<int> Post(PrintingEdition printingEdition)
        {
            await base.Post(printingEdition);
            return printingEdition.Id;
        }

        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }

        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }
}