﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.DAL.Repositories.EFRepositories
{ 
    public class UserRepository :  IUserRepository
    {
        private readonly StoreContext _storeContext;

        public UserRepository(StoreContext storeContext)
        {
            _storeContext = storeContext;
        }
        public PageIndexModel<User> GetUsers(string firstName, string lastName, string email, bool isActive = true, int page = 1)
        {
            IEnumerable<User> list = _storeContext.Users.ToList();

            Filter(ref list, firstName, lastName, email, isActive);
            
            return Paginator(list, page);
        }

        private void Filter(ref IEnumerable<User> outlist, string firstName, string lastName, string email, bool isActive)
        {
            if (firstName != null) outlist = outlist.Where(x => x.FirstName == firstName);
            if (lastName != null) outlist = outlist.Where(x => x.LastName == lastName);
            if (email != null) outlist = outlist.Where(x => x.Email == email);
            outlist = outlist.Where(x => x.IsActive == isActive);
        }

        private PageIndexModel<User> Paginator(IEnumerable<User> list, int page)
        {
            var items = list.Skip((page - 1) * Constants.PAGE_SIZE).Take(Constants.PAGE_SIZE);

            PageModel pageModel = new PageModel(list.Count(), page, Constants.PAGE_SIZE);
            PageIndexModel<User> pageIndexModel = new PageIndexModel<User>
            {
                PageModel = pageModel,
                Entities = items
            };
            return pageIndexModel;
        }

        public async Task<User> GetUser(UserManager<User> userManager, string id)
        {
            return await _storeContext.Users.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<User> GetUserByName(UserManager<User> userManager, string userName)
        {
            return await userManager.FindByNameAsync(userName);
        }

        public async Task<string> Create(UserManager<User> userManager, User model, string password)
        {
            User user = new User { Email = model.Email, UserName = model.Email, FirstName = model.FirstName, LastName = model.LastName };

            var result = await userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(user, Enums.User.UserRole.Client.ToString());
            }
            return user.Id;
        }

        public async Task Edit(UserManager<User> userManager, User model, string id)
        {
            User user = await _storeContext.Users.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);

            if (user != null)
            {
                user.Email = model.Email;
                user.UserName = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.IsActive = model.IsActive;
                user.RefreshToken = model.RefreshToken;
                user.RefreshExpires = model.RefreshExpires;

                _storeContext.Entry(user).State = EntityState.Modified;
                await _storeContext.SaveChangesAsync();
            }

        }
        public async Task Delete(UserManager<User> userManager, User model)
        {
            User user = await userManager.FindByIdAsync(model.Id);
            if (user != null)
            {
                await userManager.DeleteAsync(user);
            }
        }

        public async Task<bool> UserInRole(UserManager<User> userManager, User user, Enums.User.UserRole role)
        {
            return await userManager.IsInRoleAsync(user, role.ToString());
        }

        public async Task<IList<string>> UserRole(UserManager<User> userManager, User user)
        {
            return await userManager.GetRolesAsync(user);
        }

        public async Task SignInUser(User user, SignInManager<User> signInManager)
        {
            await signInManager.SignInAsync(user, false);
        }
        public async Task<User> AuthenticateUser(string email, string password, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            var result = await signInManager.PasswordSignInAsync(email, password, false, false);

            if (result.Succeeded)
            {
                return await userManager.FindByNameAsync(email);
            }
            return null;
        }
        public async Task SignOff(SignInManager<User> signInManager)
        {
            await signInManager.SignOutAsync();
        }

        public async Task<bool> Exists(UserManager<User> userManager, string id)
        {
            var user = await userManager.FindByIdAsync(id);

            return user == null ? false : true;
        }
    }
}