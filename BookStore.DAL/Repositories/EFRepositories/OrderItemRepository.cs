﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class OrderItemRepository : BaseEFRepository<OrderItem>, IOrdersItemRepository
    {
        private readonly StoreContext _storeContext;

        public OrderItemRepository(StoreContext storeContext)
            :base(storeContext)
        {
            _storeContext = storeContext;
        }
        public new IEnumerable<OrderItem> GetAll()
        {
            return _storeContext.OrderItems.Include("PrintingEditions").Include("Order").Include("Order.User").Include("Order.Payment");
        }

        public new async Task<OrderItem> Get(int id)
        {
            return await _storeContext.OrderItems.Include("PrintingEditions").Include("Order").FirstOrDefaultAsync(e => e.Id == id);
        }

        public new void Put(int id, OrderItem orderItem)
        {
           base.Put(id, orderItem);
        }

        public async new Task Post(OrderItem orderItem)
        {
            await base.Post(orderItem);
        }

        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }
        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }

}