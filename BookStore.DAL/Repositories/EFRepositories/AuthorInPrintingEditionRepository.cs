﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class AuthorInPrintingEditionRepository : BaseEFRepository<AuthorInPrintingEdition>, IAuthorInPrintingEdition
    {
        private readonly StoreContext _storeContext;

        public AuthorInPrintingEditionRepository(StoreContext storeContext)
            :base(storeContext)
        {
            _storeContext = storeContext;
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> Get(string name)
        {
            var author = await _storeContext.Authors.AsNoTracking().FirstOrDefaultAsync(e => e.Name == name);

            if (author == null) return null;

            return _storeContext.AuthorInPrintingEditions.Include("Author").Include("PrintingEdition").Where(e => e.Author.Id == author.Id);
        }

        public new void Put(int id, AuthorInPrintingEdition authorInPrintingEdition)
        {
            base.Put(id, authorInPrintingEdition);
        }

        public async new Task Post(AuthorInPrintingEdition authorInPrintingEdition)
        {
            await base.Post(authorInPrintingEdition);
        }

        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }
        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }
}