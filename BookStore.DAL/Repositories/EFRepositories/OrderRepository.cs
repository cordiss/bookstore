﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class OrderRepository : BaseEFRepository<Order>, IOrderRepository
    {
        private readonly StoreContext _storeContext;

        public OrderRepository(StoreContext storeContext)
            : base(storeContext)
        {
            _storeContext = storeContext;
        }
        public PageIndexModel<Order> GetAll(string id, int page = 1, Enums.Order.Status status = Enums.Order.Status.Any, Enums.Order.Sorting sorting = Enums.Order.Sorting.CreationDataAsc)
        {
            IEnumerable<Order> list = _storeContext.Orders.Include("User").Include("Payment");

            Filter(ref list, id, status);
            Sorting(ref list, sorting);

            return base.Pagination(list, page);
        }

        private void Filter(ref IEnumerable<Order> outlist, string id, Enums.Order.Status status)
        {
            if(id != null) outlist = outlist.Where(x => x.User?.Id == id);
            if(status != Enums.Order.Status.Any) outlist = outlist.Where(x => x.Status == status);
            outlist = outlist.Where(x => x.IsRemoved != true);
        }

        private void Sorting(ref IEnumerable<Order> outlist, Enums.Order.Sorting sorting)
        {
            if (sorting == Enums.Order.Sorting.CreationDataAsc) outlist = outlist.OrderBy(x => x.CreationData);
            if (sorting == Enums.Order.Sorting.CreationDataDesc) outlist = outlist.OrderByDescending(x => x.CreationData);
            if (sorting == Enums.Order.Sorting.StatusAsc) outlist = outlist.OrderBy(x => x.Status);
            if (sorting == Enums.Order.Sorting.StatusDesc) outlist = outlist.OrderByDescending(x => x.Status);
        }

        public new async Task<Order> Get(int id)
        {
            return await _storeContext.Orders.Include("User").Include("Payment").FirstOrDefaultAsync(e => e.Id == id);
        }

        public new void Put(int id, Order order)
        {
            base.Put(id, order);
        }

        public async new Task<int> Post(Order order)
        {
            await base.Post(order);
            return order.Id;
        }

        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }
        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }
}