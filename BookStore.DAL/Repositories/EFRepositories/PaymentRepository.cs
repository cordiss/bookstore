﻿using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class PaymentRepository : BaseEFRepository<Payment>, IPaymentRepository
    { 
        public PaymentRepository(StoreContext storeContext)
            :base(storeContext)
        {

        }
        public PageIndexModel<Payment> GetAll(int page = 1)
        {
            var list = base.GetAll();
            return base.Pagination(list, page);
        }

        public new Payment Get(int id)
        {
            return base.Get(id);
        }

        public new void Put(int id, Payment payment)
        {
            base.Put(id, payment);
        }

        public async new Task<int> Post(Payment payment)
        {
            await base.Post(payment);
            return payment.Id;
        }

        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }
        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }
}
