﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Infrastructure;
using BookStore.DAL.Entities;
using BookStore.DAL.Repositories.BaseRepositories;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.DAL.Repositories.EFRepositories
{
    public class AuthorRepository : BaseEFRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(StoreContext storeContext)
            :base(storeContext)
        {

        }

        public PageIndexModel<Author> GetAll(string authorName, int page = 1)
        {
            IEnumerable<Author> list = base.GetAll();

            Filter(ref list, authorName);

            return base.Pagination(list, page);
        }

        public void Filter(ref IEnumerable<Author> outlist, string authorName)
        {
            if (authorName != null) outlist = outlist.Where(x => x.Name == authorName);
            outlist = outlist.Where(x => x.IsRemoved != true);
        }

        public new Author Get(int id)
        {
            return base.Get(id);
        }

        public new void Put(int id, Author author)
        {
            base.Put(id, author);
        }

        public async new Task Post(Author author)
        {
            await base.Post(author);
        }
        
        public async new Task Delete(int id)
        {
            await base.Delete(id);
        }
        public async new Task<bool> Exists(int id)
        {
            return await base.Exists(id);
        }
    }
}