﻿namespace BookStore
{
    public partial class Constants
    {
        public const int PAGE_SIZE = 3;

        public const string ISSUER = "BookStoreAuthServer";
        public const string AUDIENCE = "https://localhost:5001/";
        public const int LIFETIME = 30;
        public const string KEY = "mePUtr3df8hXDWBMJwFNJnUL0R0N";

        public const string STORE_EMAIL = "bookstore566@gmail.com";
        public const string STORE_EMAIL_PASSWORD = "WR2eyg8l";
        public const string STORE_EMAIL_NAME = "Администрация книжного магазина";
        public const string STORE_EMAIL_HOST = "smtp.gmail.com";
        public const int STORE_EMAIL_PORT = 587;

        public const string ADMIN_EMAIL = "admin@gmail.com";
        public const string PASSWORD = "_Tve1rRx9";

        public const byte MAX_ERRORS = 100;

        public class ErrorsConstants
        {
            public const string ERROR_MODEL_NOT_FOUND = "Request Error: Model with this parameters was not found. Try again.";
            public const string ERROR_MODEL_BAD_INPUT_DATA = "Request Error: Input model was not valid.";
            public const string ERROR_MODEL_CREATION = "Request Error: Model with this parameters already exists.";
            public const string ERROR_MODEL_ACCES_DENIED = "Request Error: Acces to this model is denied.";

            public const string ERROR_ACCES_INVALID_REFRESH = "Acces Error: Refresh Token is'n valid.";
            public const string ERROR_ACCES_EMAIL_UNCONFIRMED = "Acces Error: Users email is not confirmed.";
            public const string ERROR_ACCES_DENIED = "Acces Error: Acces to resource is denied. Authentication was unsuccesful.";
            public const string ERROR_ACCES_RIGHTS = "Acces Error: Acces denied. It isn't enough rights.";
        }
    }
}