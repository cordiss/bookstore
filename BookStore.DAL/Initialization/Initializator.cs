﻿using System;
using System.Linq;
using BookStore.DAL.Entities;

namespace BookStore.DAL.Initialization
{
    public class Initializator
    {
        public static void Initialize(StoreContext context)
        {            
            if (!(context.Authors.Any()))
            {
                context.Authors.AddRange(
                    new Author
                {
                    Name = "A. S. Pushkin",
                    CreationData = DateTime.Now,
                    IsRemoved = false
                });
                context.SaveChanges();
            }
            if (!(context.PrintingEditions.Any()))
            {
                context.PrintingEditions.AddRange(
                    new PrintingEdition
                {
                    Title = "Some Title",
                    Description = "Some Descripton",
                    Price = 5.5,
                    Currency = Enums.PrintingEdition.Currency.USD,
                    Type = Enums.PrintingEdition.Type.Book
                });
                context.SaveChanges();
            }
            if (!(context.AuthorInPrintingEditions.Any()))
            {
                context.AuthorInPrintingEditions.AddRange(
                    new AuthorInPrintingEdition
                    {
                        Author = context.Authors.FirstOrDefault(),
                        PrintingEdition = context.PrintingEditions.FirstOrDefault()
                    }
                );
                context.SaveChanges();
            }

            if (!(context.Payments.Any()))
            {
                context.Payments.AddRange(
                    new Payment
                    {
                        TransactionId = 123456789,
                        CreationData = DateTime.Now,
                        IsRemoved = false
                    }
                );
                context.SaveChanges();
            }

            if (!(context.Orders.Any()))
            {
                context.Orders.AddRange(
                    new Order
                    {
                        Description = "Closed",
                        User = context.Users.FirstOrDefault(),
                        Payment = context.Payments.FirstOrDefault(),
                        Status = Enums.Order.Status.Paid,
                        CreationData = DateTime.Now
                    }
                );
                context.SaveChanges();
            }
                 
            if (!(context.OrderItems.Any()))
            {
                context.OrderItems.AddRange(
                    new OrderItem
                    {
                        Amount = context.PrintingEditions.FirstOrDefault().Price,
                        PrintingEditions = context.PrintingEditions.FirstOrDefault(),
                        Currency = Enums.PrintingEdition.Currency.USD,
                        Order = context.Orders.FirstOrDefault(),
                        Count = 1
                    }
                );
                context.SaveChanges();
            }
        }
    }
}