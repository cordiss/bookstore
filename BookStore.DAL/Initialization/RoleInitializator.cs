﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using BookStore.DAL.Entities;

namespace BookStore.DAL.Initialization
{
    public class RoleInitializator
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await roleManager.FindByNameAsync("client") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("client"));
            }         
            if (await userManager.FindByNameAsync(Constants.ADMIN_EMAIL) == null)
            {
                User admin = new User { FirstName = "Admin", LastName = "Admin", Email = Constants.ADMIN_EMAIL, UserName = Constants.ADMIN_EMAIL, EmailConfirmed = true };
                IdentityResult result = await userManager.CreateAsync(admin, Constants.PASSWORD);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, Enums.User.UserRole.Admin.ToString());
                }
            }
        }
    }
}