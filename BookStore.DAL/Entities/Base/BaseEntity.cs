﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookStore.DAL.Entities.Base
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationData { get; set; }
        public bool IsRemoved { get; set; }

        public List<ErrorsModel> Errors = new List<ErrorsModel>();
    }
}
