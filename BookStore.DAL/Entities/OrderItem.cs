﻿namespace BookStore.DAL.Entities
{
    public class OrderItem : Base.BaseEntity
    {
        public double Amount { get; set; }
        public virtual PrintingEdition PrintingEditions { get; set; }
        public Enums.PrintingEdition.Currency Currency { get; set; }
        public virtual Order Order { get; set; }
        public int Count { get; set; }
    }
}