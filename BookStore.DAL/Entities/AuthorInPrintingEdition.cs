﻿namespace BookStore.DAL.Entities
{
    public class AuthorInPrintingEdition : Base.BaseEntity
    {
        public virtual Author Author { get; set; }
        public virtual PrintingEdition PrintingEdition { get; set; }
    }
}