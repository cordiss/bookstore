﻿namespace BookStore.DAL.Entities
{
    public class PrintingEdition : Base.BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public Enums.PrintingEdition.Currency Currency { get; set; }
        public Enums.PrintingEdition.Type Type { get; set; }
    }
}