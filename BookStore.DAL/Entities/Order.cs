﻿using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class Order : Base.BaseEntity
    {
        public string Description { get; set; }
        public virtual User User { get; set; }
        public virtual Payment Payment { get; set; }
        public Enums.Order.Status Status { get; set; }
        public double Amount { get; set; }

        public ICollection<OrderItem> Orders = new List<OrderItem>();
    }
}