﻿using System;
using Microsoft.AspNetCore.Identity;

namespace BookStore.DAL.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshExpires { get; set; }
        public bool IsActive { get; set; }
    }
}