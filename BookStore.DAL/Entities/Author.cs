﻿namespace BookStore.DAL.Entities
{
    public class Author : Base.BaseEntity
    {
        public string Name { get; set; }
    }
}
