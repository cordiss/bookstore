﻿using System;
using System.Net;
using System.Collections.Generic;

namespace BookStore
{
    public partial class ErrorsModel
    {
        public string Message { get; set; }
        public DateTime ErrorDate { get; set; }
        public string ErrorPlace { get; set; }
        public bool IsNew { get; set; }
        public HttpStatusCode StatusCode { get; set; }        

        public static List<ErrorsModel> Errors = new List<ErrorsModel>(); 
    }
}