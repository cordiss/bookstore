﻿using System.Collections.Generic;

namespace BookStore.DAL.Infrastructure
{
    public class PageIndexModel<T> where T: class
    {
        public IEnumerable<T> Entities { get; set; }
        public PageModel PageModel { get; set; }
    }
}