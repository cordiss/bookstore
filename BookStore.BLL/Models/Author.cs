﻿namespace BookStore.BLL.Models
{
    public class Author : Base.BaseModel
    {
        public string Name { get; set; }
    }
}
