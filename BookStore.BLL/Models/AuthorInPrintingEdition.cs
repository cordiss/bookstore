﻿namespace BookStore.BLL.Models
{
    public class AuthorInPrintingEdition : Base.BaseModel
    {
        public Author Author { get; set; }
        public PrintingEdition PrintingEdition { get; set; }
    }
}