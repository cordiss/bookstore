﻿namespace BookStore.BLL
{
    public static partial class Enums
    {
        public static class User
        {
            public enum UserRole
            {
                Admin,
                Client
            }
        }
        public static class PrintingEdition
        {
            public enum Currency
            {
                USD,
                EUR,
                GBP,
                UAH,
                CHF,
                JPY
            }
            public enum Type
            {
                Any = 0,
                Journal = 1,
                Newspaper,
                Book
            }
            public enum SortingMethod
            {
                TitleAsc = 1,
                TitleDesc,
                PriceAsc,
                PriceDesc,
                CreationDataAsc,
                CreationDataDesc
            }
        }

        public static class Order
        {
            public enum Status
            {
                Any = 2,
                Paid = 1,
                UnPaid = 0
            }
            public enum Sorting
            {
                CreationDataAsc,
                CreationDataDesc,
                StatusAsc,
                StatusDesc
            }
        }
    }
}