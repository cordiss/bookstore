﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace BookStore.BLL.Models.Base
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationData { get; set; }
        public bool IsRemoved { get; set; }

        public List<ErrorsModel> Errors = new List<ErrorsModel>();
    }
}
