﻿using System.Collections.Generic;

namespace BookStore.BLL.Models
{
    public class Order : Base.BaseModel
    {
        public string Description { get; set; }
        public User User { get; set; }
        public Payment Payment { get; set; }
        public Enums.Order.Status Status { get; set; }
        public double Amount { get; set; }


        public ICollection<OrderItem> Orders = new List<OrderItem>();
    }
}