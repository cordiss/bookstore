﻿namespace BookStore.BLL.Models
{
    public class OrderItem : Base.BaseModel
    {
        public double Amount { get; set; }
        public PrintingEdition PrintingEditions { get; set; }
        public Enums.PrintingEdition.Currency Currency { get; set; }
        public Order Order { get; set; }
        public int Count { get; set; }
    }
}