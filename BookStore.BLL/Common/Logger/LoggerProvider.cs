﻿using Microsoft.Extensions.Logging;

namespace BookStore.BLL.Common.Logger
{
    public class LoggerProvider : ILoggerProvider
    {
        private readonly string _path;

        public LoggerProvider(string path)
        {
            _path = path;
        }

        public ILogger CreateLogger(string categoryName) => new Logger(_path);

        public void Dispose() { }
    }
}