﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.EFRepositories;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.BLL.Services
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _authorRepository;
        private readonly StoreContext _storeContext;

        public AuthorService(StoreContext storeContext)
        {
            _authorRepository = new AuthorRepository(storeContext);
            _storeContext = storeContext;
        }

        public (IEnumerable<Author>, int, int) GetAuthors(string authorName, int page = 1)
        {
            var result = _authorRepository.GetAll(authorName, page);

            var authorMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Author, Author>()).CreateMapper();
            var bllAuthor = authorMapper.Map<IEnumerable<DAL.Entities.Author>, List<Author>>(result.Entities);

            return (bllAuthor, result.PageModel.TotalPages, result.PageModel.PageNumber);
        }

        public Author GetAuthor(int? id)
        {
            var author = _authorRepository.Get(id.Value);

            #region Validation
            if (author == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound});

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }
            #endregion

            return new Author { Name = author?.Name, Id = author.Id, CreationData = author.CreationData, IsRemoved = author.IsRemoved };            
        }

        public async Task<bool> PutAuthor(int? id, Author putAuthor)
        {
            #region Validation
            if (!(await AuthorExists(putAuthor.Id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if (putAuthor == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            DAL.Entities.Author dalAuthor = new DAL.Entities.Author
            {
                Id = putAuthor.Id,
                Name = putAuthor.Name,
                CreationData = putAuthor.CreationData,
                IsRemoved = putAuthor.IsRemoved
            };

            _authorRepository.Put(id.Value, dalAuthor);
            return true;
        }

        public async Task<bool> PostAuthor(Author author)
        {
            #region Validation
            if (author == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if (await AuthorExists(author.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            DAL.Entities.Author dalAuthor = new DAL.Entities.Author
            {
                Id = author.Id,
                Name = author.Name,
                CreationData = DateTime.Now,
                IsRemoved = author.IsRemoved
            };

            await _authorRepository.Post(dalAuthor);
            return true;
        }

        public bool DeleteAuthor(int? id)
        {
            var entity = _storeContext.Authors.Find(id.Value);

            #region Validation
            if (entity == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.Authors SET IsRemoved = 'true' WHERE Id = {id.Value}");
            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.AuthorInPrintingEditions SET IsRemoved = 'true' WHERE AuthorId = {id.Value}");

            var authorsInPE = _storeContext.AuthorInPrintingEditions.Include("PrintingEdition").Where(x => x.Author.Id == id.Value).ToList();

            foreach(DAL.Entities.AuthorInPrintingEdition d in authorsInPE)
            {
                _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.PrintingEditions SET IsRemoved = 'true' WHERE Id = {d.PrintingEdition.Id}");
            }
            _storeContext.SaveChanges();
            return true;
        }

        public async Task<bool> AuthorExists(int? id)
        {
            return await _authorRepository.Exists(id.Value);
        }
    }
}