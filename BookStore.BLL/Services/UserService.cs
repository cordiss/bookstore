﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.EFRepositories;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly SignInManager<DAL.Entities.User> _signInManager;
        private readonly UserManager<DAL.Entities.User> _userManager;

        private readonly RoleManager<IdentityRole> _roleManager;

        private IUserRepository _userRepository;

        private readonly StoreContext _storeContext;

        public UserService(UserManager<DAL.Entities.User> userManager, RoleManager<IdentityRole> roleManager, SignInManager<DAL.Entities.User> signInManager, StoreContext storeContext)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _storeContext = storeContext;
            _userRepository = new UserRepository(_storeContext);
        }

        public (IEnumerable<User>, int, int) GetUsers(string firstName, string lastName, string email, bool isActive = true, int page = 1)
        {
            var result = _userRepository.GetUsers(firstName, lastName, email, isActive, page);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap <DAL.Entities.User, User>()).CreateMapper();
            var bllUsers = mapper.Map<IEnumerable<DAL.Entities.User>, List<User>>(result.Entities);

            return (bllUsers, result.PageModel.TotalPages, result.PageModel.PageNumber);
        }

        public async Task<User> GetUser(string id)
        {
            #region Validation
            if (!(await Exists(id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            #endregion

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();

            return mapper.Map<DAL.Entities.User, User>(await _userRepository.GetUser(_userManager, id));
        }
        public async Task<User> GetUserByName(string userName)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();

            return mapper.Map<DAL.Entities.User, User>(await _userRepository.GetUserByName(_userManager, userName));
        }

        public async Task<string> GetUserId(string userName)
        {
            var user = await _storeContext.Users.AsNoTracking().FirstOrDefaultAsync(x => x.UserName == userName);

            return user.Id;
        }

        public async Task<string> Create(User model, string password)
        {
            #region Validation
            if (model == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            if (await Exists(model.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            #endregion

            var dalUser = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();
            var createUser = dalUser.Map<User, DAL.Entities.User>(model);

            var result = await _userRepository.Create(_userManager, createUser, password);

            return result;
        }

        public async Task<string> GenerateEmailToken(string id)
        {
            DAL.Entities.User user = await _userManager.FindByIdAsync(id);

            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            code = System.Web.HttpUtility.UrlEncode(code);

            return code;
        }

        public async Task<IdentityResult> ConfirmEmailAddress(string userId, string code)
        {
            DAL.Entities.User user = await _userManager.FindByIdAsync(userId);

            return await _userManager.ConfirmEmailAsync(user, code);
        }

        public async Task<(string, string)> GenerateResetPasswordToken(string email)
        {
            DAL.Entities.User user = await _userManager.FindByNameAsync(email);

            #region Validation
            if (user == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return (null, null);
            }

            if(!(await _userManager.IsEmailConfirmedAsync(user)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_EMAIL_UNCONFIRMED, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.Forbidden });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return (null, null);
            }
            #endregion

            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

            code = System.Web.HttpUtility.UrlEncode(code);
            return (user.Id, code);
        }
        public async Task<IdentityResult> ConfirmResetPassword(string userId, string code, string newPassword)
        {
            DAL.Entities.User user = await _userManager.FindByIdAsync(userId);

            return await _userManager.ResetPasswordAsync(user, code, newPassword);
        }

        public async Task<bool> Edit(string id, User model)
        {
            #region Validation
            if (model == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }
            #endregion

            var dalUser = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();

            await _userRepository.Edit(_userManager, dalUser.Map<User, DAL.Entities.User>(model), id);
            return true;
        }
        public async Task<bool> Delete(User model)
        {
            #region Validation
            if (model == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }
            if (!(await Exists(model.Id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }
            #endregion

            var managerMapper = new MapperConfiguration(cfg => cfg.CreateMap<UserManager<User>, UserManager<DAL.Entities.User>>()).CreateMapper();
            var dalUser = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();

            await _userRepository.Delete(_userManager, dalUser.Map<User, DAL.Entities.User>(model));
            return true;
        }

        public async Task<IList<string>> UserRole(User user)
        {
            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();
            var dalUser = userMapper.Map<User, DAL.Entities.User>(user);

            return await _userRepository.UserRole(_userManager, dalUser);
        }

        public async Task<bool> UserInRole(User user, Enums.User.UserRole role)
        {
            var exists = _roleManager.FindByNameAsync(role.ToString());

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();
            var dalUser = userMapper.Map<User, DAL.Entities.User>(user);

            return await _userRepository.UserInRole(_userManager, dalUser, (DAL.Enums.User.UserRole)role);
        }

        public async Task<User> AuthenticateUser(string Email, string Password)
        {
            #region Validation
            if (Email == null || Password == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            #endregion

            DAL.Entities.User usr = await _userManager.FindByNameAsync(Email);

            #region Validation
            if (!(await _userManager.IsEmailConfirmedAsync(usr)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_EMAIL_UNCONFIRMED, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.Forbidden });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            #endregion

            var dalUser = await _userRepository.AuthenticateUser(Email, Password, _signInManager, _userManager);

            #region Validation
            if (dalUser == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_DENIED, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.Forbidden });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }
            #endregion

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var user = userMapper.Map<DAL.Entities.User, User>(dalUser);
            return user;
        }

        public async Task<bool> SignInUser(User user)
        {
            #region Validation
            if (user == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }
            #endregion

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();
            var dalUser = userMapper.Map<User, DAL.Entities.User>(user);

            await _userRepository.SignInUser(dalUser, _signInManager);
            return true;
        }

        public async Task SignOff()
        {
            await _userRepository.SignOff(_signInManager);
        }

        public async Task<bool> BlockUser(string id, string username)
        {
            var user = await _userManager.FindByIdAsync(id);
            var currentUser = await _userManager.FindByNameAsync(username);

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var dalCurrentUser = userMapper.Map<DAL.Entities.User, User>(currentUser);

            #region Validation
            if (!(await UserInRole(dalCurrentUser, Enums.User.UserRole.Admin)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_RIGHTS, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.Forbidden });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }

            if (user == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return false;
            }
            #endregion

            user.IsActive = !user.IsActive;

            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.AspNetUsers SET IsActive = '{user.IsActive}' WHERE UserName = '{user.UserName}'");
            _storeContext.SaveChanges();
            return true;
        }

        private async Task<bool> Exists(string id)
        {
            var managerMapper = new MapperConfiguration(cfg => cfg.CreateMap<UserManager<User>, UserManager<DAL.Entities.User>>()).CreateMapper();
            var dalUser = new MapperConfiguration(cfg => cfg.CreateMap<User, DAL.Entities.User>()).CreateMapper();

            return await _userRepository.Exists(_userManager, id);
        }
    }
}