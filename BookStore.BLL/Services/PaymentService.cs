﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.EFRepositories;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.BLL.Services
{
    public class PaymentService : IPaymentSerivce
    {
        private IPaymentRepository _paymentRepository;

        public PaymentService(StoreContext _storeContext)
        {
            _paymentRepository = new PaymentRepository(_storeContext);
        }

        public (IEnumerable<Payment>, int, int) GetPayments(int page = 1)
        {
            var result = _paymentRepository.GetAll(page);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
            var bllPayments = mapper.Map<IEnumerable<DAL.Entities.Payment>, List<Payment>>(result.Entities);

            return (bllPayments, result.PageModel.TotalPages, result.PageModel.PageNumber);
        }

        public Payment GetPayment(int? id)
        {
            var DALpayment = _paymentRepository.Get(id.Value);

            #region Validation
            if (DALpayment == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }
            #endregion

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();

            return mapper.Map<DAL.Entities.Payment, Payment>(DALpayment);
        }

        public async Task<bool> PutPayment(int? id, Payment payment)
        {
            #region Validation
            if (payment == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if(!(await PaymentExists(id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            DAL.Entities.Payment pay = new DAL.Entities.Payment
            {
                CreationData = payment.CreationData,
                IsRemoved = payment.IsRemoved,
                TransactionId = payment.TransactionId
            };

            _paymentRepository.Put(id.Value, pay);
            return true;
        }

        public async Task<int> PostPayment(Payment payment)
        {
            #region Validation
            if (payment == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }
            if (await PaymentExists(payment.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }
            #endregion

            var transactionId = CreateTransactionId();

            DAL.Entities.Payment pay = new DAL.Entities.Payment
            {
                CreationData = DateTime.Now,
                IsRemoved = false,
                TransactionId = transactionId
            };

            return await _paymentRepository.Post(pay);
        }

        public bool DeletePayment(int? id)
        {
            var entity = _paymentRepository.Get(id.Value);

            #region Validation
            if (entity == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var payment = new DAL.Entities.Payment
            {
                Id = entity.Id,
                TransactionId = entity.TransactionId,
                CreationData = entity.CreationData,
                IsRemoved = true
            };
            _paymentRepository.Put(id.Value, payment);
            return true;
        }

        public async Task<bool> PaymentExists(int? id)
        {
            return await _paymentRepository.Exists(id.Value);
        }

        private long CreateTransactionId()
        {
            Random rnd = new Random();

            return rnd.Next(1000000, 99999999);
        }
    }
}
