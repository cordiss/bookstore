﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.EFRepositories;
using BookStore.DAL.Repositories.Interfaces;

namespace BookStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private IOrderRepository _orderRepository;
        private readonly IOrderItemService _orderItemService;
        private readonly StoreContext _storeContext;

        public OrderService(StoreContext storeContext, IOrderItemService orderItemService)
        {
            _orderRepository = new OrderRepository(storeContext);
            _storeContext = storeContext;
            _orderItemService = orderItemService;
        }

        public (IEnumerable<Order>, IEnumerable<OrderItem>, int, int) GetOrders(string userName, int page = 1, Enums.Order.Status status = Enums.Order.Status.Any, Enums.Order.Sorting sorting = Enums.Order.Sorting.CreationDataAsc)
        {
            var result = _orderRepository.GetAll(userName, page, (DAL.Enums.Order.Status)status, (DAL.Enums.Order.Sorting)sorting);

            var list = result.Entities.ToList();
            var totalPages = result.PageModel.TotalPages;
            var currentPage = result.PageModel.PageNumber;

            List<Order> outList = new List<Order>();
            List<OrderItem> outListItems = new List<OrderItem>();

            foreach (DAL.Entities.Order oI in list)
            {
                var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
                var dalUser = userMapper.Map<DAL.Entities.User, User>(oI.User);

                var payMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
                var dalPay = payMapper.Map<DAL.Entities.Payment, Payment>(oI.Payment);

                var order = new Order
                {
                    User = dalUser,
                    Payment = dalPay,
                    Description = oI.Description,
                    Amount = oI.Amount,
                    Status = (Enums.Order.Status)oI.Status,
                    Id = oI.Id,
                    CreationData = oI.CreationData,
                    IsRemoved = oI.IsRemoved
                };

                outListItems.AddRange(_orderItemService.GetOrderItems().Where(x => x.Order.Id == oI.Id));

                outList.Add(order);
            }

            return (outList, outListItems, totalPages, currentPage);
        }

        public async Task<(Order, IEnumerable<OrderItem>)> GetOrder(int? id, string userName = null)
        {
            var DALorder = await _orderRepository.Get(id.Value);

            #region Validation
            if (userName != null)
            {
                if (DALorder.User.UserName != userName)
                {
                    if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                    ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_ACCES_DENIED, ErrorDate = DateTime.Now, IsNew = true, ErrorPlace = this.ToString(), StatusCode = HttpStatusCode.Forbidden });
                    ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                    return (null, null);
                }
            }

            if (DALorder == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, IsNew = true, ErrorPlace = this.ToString(), StatusCode = HttpStatusCode.NotFound });
                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return (null, null);
            }
            #endregion

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var dalUser = userMapper.Map<DAL.Entities.User, User>(DALorder.User);

            var payMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
            var dalPay = payMapper.Map<DAL.Entities.Payment, Payment>(DALorder.Payment);

            var order = new Order
            {
                User = dalUser,
                Payment = dalPay,
                Description = DALorder.Description,
                Amount = DALorder.Amount,
                Status = (Enums.Order.Status)DALorder.Status,
                Id = DALorder.Id,
                CreationData = DALorder.CreationData,
                IsRemoved = DALorder.IsRemoved
            };

            var orderItems = _orderItemService.GetOrderItems().Where(x => x.Order.Id == DALorder.Id);

            return (order, orderItems);
        }

        public async Task<bool> PutOrder(int? id, Order order)
        {
            #region Validation
            if (order == null) 
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if (!(await OrderExists(id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var user = await _storeContext.Users.FindAsync(order.User.Id);
            if (user == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            var pay = await _storeContext.Payments.FindAsync(order.Payment.Id);
            if (pay == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var orderr = new DAL.Entities.Order
            {
                User = user,
                Payment = pay,
                Description = order.Description,
                Status = (DAL.Enums.Order.Status)order.Status,
                Id = order.Id,
                Amount = order.Amount,
                CreationData = order.CreationData,
                IsRemoved = order.IsRemoved
            };

            _orderRepository.Put(id.Value, orderr);
            return true;
        }

        public async Task<int> PostOrder(Order order)
        {
            #region Validation
            if (order == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }

            if (await OrderExists(order.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }

            var user = await _storeContext.Users.FindAsync(order.User.Id);
            if (user == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }
            var pay = await _storeContext.Payments.FindAsync(order.Payment.Id);
            if (pay == null) 
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }
            #endregion

            var orderr = new DAL.Entities.Order
            {
                User = user,
                Payment = pay,
                Description = order.Description,
                Status = (DAL.Enums.Order.Status)order.Status,
                Id = order.Id,
                Amount = order.Amount,
                CreationData = DateTime.Now,
                IsRemoved = order.IsRemoved
            };

            return await _orderRepository.Post(orderr);
        }

        public async Task<bool> DeleteOrder(int? id)
        {
            var entity = await _storeContext.Orders.FindAsync(id.Value);

            if (entity == null) 
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.Orders SET IsRemoved = 'true' WHERE Id = {id.Value}");
            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.OrderItems SET IsRemoved = 'true' WHERE OrderId = {id.Value}");
            _storeContext.SaveChanges();
            return true;
        }

        public async Task<bool> OrderExists(int? id)
        {
            return await _orderRepository.Exists(id.Value);
        }
    }
}