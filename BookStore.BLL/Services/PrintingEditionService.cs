﻿using System.Net;
using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using System.Threading.Tasks;
using BookStore.DAL.Repositories.Interfaces;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.EFRepositories;

namespace BookStore.BLL.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        private IPrintingEditionRepository _printingEditionRepository;

        public PrintingEditionService(StoreContext storeContext)
        {
            _printingEditionRepository = new PrintingEditionRepository(storeContext);
        }

        public (IEnumerable<PrintingEdition>, int, int) GetPrintingEditions(string title, string author, double minPrice = 0.0, double maxPrice = 99999.99, Enums.PrintingEdition.SortingMethod method = Enums.PrintingEdition.SortingMethod.CreationDataAsc, string type = "any", int page = 1)
        {
            var pageIndexModel = _printingEditionRepository.GetAll(title, author, type, minPrice, maxPrice, (DAL.Enums.PrintingEdition.SortingMethod)method, page);

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.PrintingEdition, PrintingEdition>()).CreateMapper();
            var pe = mapper.Map<IEnumerable<DAL.Entities.PrintingEdition>, IEnumerable<PrintingEdition>>(pageIndexModel.Entities);

            return (pe, pageIndexModel.PageModel.TotalPages, pageIndexModel.PageModel.PageNumber);
        }

        public PrintingEdition GetPrintingEdition(int? id)
        {
            var PrintingEdition = _printingEditionRepository.Get(id.Value);

            #region Validation
            if (PrintingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }
            #endregion

            return new PrintingEdition { Title = PrintingEdition.Title, Id = PrintingEdition.Id, CreationData = PrintingEdition.CreationData, IsRemoved = PrintingEdition.IsRemoved, Description = PrintingEdition.Description, Currency = (Enums.PrintingEdition.Currency)PrintingEdition.Currency, Price = PrintingEdition.Price, Type = (Enums.PrintingEdition.Type)PrintingEdition.Type };
        }

        public async Task<bool> PutPrintingEdition(int? id, PrintingEdition printingEdition)
        {
            #region Validation
            if (printingEdition == null) 
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            if (!(await PrintingEditionExists(id))) 
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            DAL.Entities.PrintingEdition dalPrintingEdition = new DAL.Entities.PrintingEdition
            {
                Id = printingEdition.Id,
                Title = printingEdition.Title,
                CreationData = printingEdition.CreationData,
                IsRemoved = printingEdition.IsRemoved,
                Description = printingEdition.Description,
                Currency = (DAL.Enums.PrintingEdition.Currency)printingEdition.Currency,
                Type = (DAL.Enums.PrintingEdition.Type)printingEdition.Type,                
                Price = printingEdition.Price
            };

            _printingEditionRepository.Put(id.Value, dalPrintingEdition);
            return true;
        }

        public async Task<int> PostPrintingEdition(PrintingEdition printingEdition)
        {
            #region Validation
            if (printingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }

            if (await PrintingEditionExists(printingEdition.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return -1;
            }
            #endregion

            DAL.Entities.PrintingEdition dalPrintingEdition = new DAL.Entities.PrintingEdition
            {
                Id = printingEdition.Id,
                Title = printingEdition.Title,
                CreationData = DateTime.Now,
                IsRemoved = printingEdition.IsRemoved,
                Description = printingEdition.Description,
                Currency = (DAL.Enums.PrintingEdition.Currency)printingEdition.Currency,
                Type = (DAL.Enums.PrintingEdition.Type)printingEdition.Type,
                Price = printingEdition.Price
            };

            return await _printingEditionRepository.Post(dalPrintingEdition);
        }

        public bool DeletePrintingEdition(int id)
        {
            var entity = _printingEditionRepository.Get(id);

            #region Validation
            if (entity == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            DAL.Entities.PrintingEdition pe = new DAL.Entities.PrintingEdition
            {
                Id = entity.Id,
                Title = entity.Title,
                Description = entity.Description,
                Currency = entity.Currency,
                Type = entity.Type,
                Price = entity.Price,
                CreationData = entity.CreationData,
                IsRemoved = true
            };
            _printingEditionRepository.Put(id, pe);
            
            return true;
        }
        public async Task<bool> PrintingEditionExists(int? id)
        {
            return await _printingEditionRepository.Exists(id.Value);
        }
    }
}