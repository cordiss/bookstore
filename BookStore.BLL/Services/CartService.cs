﻿using System;
using System.Net;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;

namespace BookStore.BLL.Services
{
    public class CartService : ICartService
    {
        private readonly StoreContext _storeContext;
        private readonly IOrderService _orderService;
        private readonly IOrderItemService _orderItemSerivce;
        private readonly IPaymentSerivce _paymentSerivce;

        public CartService(StoreContext storeContext, IOrderService orderService, IOrderItemService orderItemService, IPaymentSerivce paymentService)
        {
            _storeContext = storeContext;
            _orderService = orderService;
            _orderItemSerivce = orderItemService;
            _paymentSerivce = paymentService;
        }

        public async Task<int> CreateOrder(string username)
        {
            var user = await _storeContext.Users.AsNoTracking().FirstOrDefaultAsync(e => e.UserName == username);

            var lastOrder = await _storeContext.Orders.Include("User").Where(e => e.Description != "Closed").Where(e => e.IsRemoved != true).FirstOrDefaultAsync(e => e.User.UserName == username);

            if (lastOrder != null) return lastOrder.Id;

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var dalUser = userMapper.Map<DAL.Entities.User, User>(user);

            int payID = await _paymentSerivce.PostPayment(new Payment { CreationData = DateTime.Now, IsRemoved = false });
            var pay = _paymentSerivce.GetPayment(payID);

            var order = new Order
            {
                User = dalUser,
                Payment = pay,
                CreationData = DateTime.Now,
                IsRemoved = false,
                Description = "New Order",
                Amount = 0,
                Status = Enums.Order.Status.UnPaid
            };

            return await _orderService.PostOrder(order);
        }

        public async Task<bool> AddOrderItem(int printingEditionId, int orderId, int count)
        {
            var result = await _orderService.GetOrder(orderId);

            var order = result.Item1;
            var orderItems = result.Item2;

            #region Validation
            if (order == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var printingEdition = await _storeContext.PrintingEditions.AsNoTracking().FirstOrDefaultAsync(e => e.Id == printingEditionId);
            if (printingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var orderItemsExisting = orderItems.Where(x => x.PrintingEditions.Id == printingEdition.Id).ToList();

            if (orderItemsExisting.Count != 0)
            {
                _storeContext.Database.ExecuteSqlRaw("UPDATE dbo.Orders SET Amount = {0} WHERE Id = {1}", order.Amount + (printingEdition.Price * count), orderId);
                _storeContext.Database.ExecuteSqlRaw("UPDATE dbo.OrderItems SET Count = {0}, Amount = {1} WHERE Id = {2}", orderItemsExisting.First().Count + count, orderItemsExisting.First().Amount + (printingEdition.Price * count), orderItemsExisting.First().Id);
                _storeContext.SaveChanges();
                return true;
            }

            var peMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.PrintingEdition, PrintingEdition>()).CreateMapper();
            var dalPE = peMapper.Map<DAL.Entities.PrintingEdition, PrintingEdition>(printingEdition);

            var amount = dalPE.Price * count;

            var orderItem = new OrderItem
            {
                PrintingEditions = dalPE,
                Order = order,
                CreationData = DateTime.Now,
                Count = count,
                Amount = amount,
                Currency = dalPE.Currency,
                IsRemoved = false
            };

            await _orderItemSerivce.PostOrderItem(orderItem);

            _storeContext.Database.ExecuteSqlRaw("UPDATE dbo.Orders SET Amount = {0} WHERE Id = {1}", order.Amount + orderItem.Amount, orderId);
            _storeContext.SaveChanges();
            return true;
        }

        public async Task<bool> ConfirmOrder(int orderId, string username)
        {
            var lastOrder = await _storeContext.Orders.Include("User").Where(e => e.Description != "Closed").FirstOrDefaultAsync(e => e.User.UserName == username);

            #region Validation
            if (lastOrder == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var dalUser = userMapper.Map<DAL.Entities.User, User>(lastOrder.User);

            var paymentMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
            var dalPayment = userMapper.Map<DAL.Entities.Payment, Payment>(lastOrder.Payment);

            lastOrder.Description = "Closed";

            _storeContext.Entry(lastOrder).State = EntityState.Modified;
            _storeContext.SaveChanges();
            return true;
        }
    }
}