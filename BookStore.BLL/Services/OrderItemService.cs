﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.Interfaces;
using BookStore.DAL.Repositories.EFRepositories;

namespace BookStore.BLL.Services
{
    public class OrderItemService : IOrderItemService
    {
        private IOrdersItemRepository _orderItemRepository;
        private readonly StoreContext _storeContext;
        public OrderItemService(StoreContext storeContext)
        {
            _orderItemRepository = new OrderItemRepository(storeContext);
            _storeContext = storeContext;
        }

        public IEnumerable<OrderItem> GetOrderItems()
        {
            var list = _orderItemRepository.GetAll().ToList();
            List<OrderItem> outList = new List<OrderItem>();

            foreach (DAL.Entities.OrderItem oI in list)
            {
                var peMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.PrintingEdition, PrintingEdition>()).CreateMapper();
                var dalPE = peMapper.Map<DAL.Entities.PrintingEdition, PrintingEdition>(oI.PrintingEditions);

                var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
                var dalUser = userMapper.Map<DAL.Entities.User, User>(oI.Order.User);

                var payMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
                var dalPay = payMapper.Map<DAL.Entities.Payment, Payment>(oI.Order.Payment);

                var getOrder = new Order
                {
                    User = dalUser,
                    Payment = dalPay,
                    Description = oI.Order.Description,
                    Status = (Enums.Order.Status)oI.Order.Status,
                    Id = oI.Order.Id,
                    Amount = oI.Amount,
                    CreationData = oI.Order.CreationData,
                    IsRemoved = oI.Order.IsRemoved
                };

                var orderItem = new OrderItem
                {
                    PrintingEditions = dalPE,
                    Order = getOrder,
                    Amount = oI.Amount,
                    Count = oI.Count,
                    Currency = (Enums.PrintingEdition.Currency)oI.Currency,
                    Id = oI.Id,
                    CreationData = oI.CreationData,
                    IsRemoved = oI.IsRemoved
                };

                outList.Add(orderItem);
            }

            return outList;
        }

        public async Task<OrderItem> GetOrderItem(int? id)
        {
            var DALorderItem = await _orderItemRepository.Get(id.Value);

            if (DALorderItem == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }

            var peMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.PrintingEdition, PrintingEdition>()).CreateMapper();
            var dalPE = peMapper.Map<DAL.Entities.PrintingEdition, PrintingEdition>(DALorderItem.PrintingEditions);

            var order = await _storeContext.Orders.FirstOrDefaultAsync(e => e.Id == DALorderItem.Order.Id);

            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, User>()).CreateMapper();
            var dalUser = userMapper.Map<DAL.Entities.User, User>(order.User);

            var payMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Payment, Payment>()).CreateMapper();
            var dalPay = payMapper.Map<DAL.Entities.Payment, Payment>(order.Payment);

            var bllOrder = new Order
            {
                User = dalUser,
                Payment = dalPay,
                Description = order.Description,
                Amount = order.Amount,
                Status = (Enums.Order.Status)order.Status,
                CreationData = order.CreationData,
                IsRemoved = order.IsRemoved,
                Id = order.Id
            };

            var outOrderItem = new OrderItem
            {
                PrintingEditions = dalPE,
                Order = bllOrder,
                Amount = DALorderItem.Amount,
                Count = DALorderItem.Count,
                Currency = (Enums.PrintingEdition.Currency)DALorderItem.Currency,
                Id = DALorderItem.Id,
                CreationData = DALorderItem.CreationData,
                IsRemoved = DALorderItem.IsRemoved
            };
            return outOrderItem;
        }

        public async Task<bool> PutOrderItem(int? id, OrderItem orderItem)
        {
            #region Validation
            if (orderItem == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if(!(await OrderItemExists(id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var pe = await _storeContext.PrintingEditions.FindAsync(orderItem.PrintingEditions.Id);
            if (pe == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            var order = await _storeContext.Orders.FindAsync(orderItem.Order.Id);
            if (order == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var outOrderItem = new DAL.Entities.OrderItem
            {
                PrintingEditions = pe,
                Order = order,
                Amount = orderItem.Amount,
                Count = orderItem.Count,
                Currency = (DAL.Enums.PrintingEdition.Currency)orderItem.Currency,
                Id = orderItem.Id,
                CreationData = orderItem.CreationData,
                IsRemoved = orderItem.IsRemoved
            };

            _orderItemRepository.Put(id.Value, outOrderItem);
            return true;
        }

        public async Task<bool> PostOrderItem(OrderItem orderItem)
        {
            #region Validation
            if (orderItem == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            if (await OrderItemExists(orderItem.Id))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_CREATION, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var pe = await _storeContext.PrintingEditions.FindAsync(orderItem.PrintingEditions.Id);
            if(pe == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            var order = await _storeContext.Orders.FindAsync(orderItem.Order.Id);
            if (order == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var outOrderItem = new DAL.Entities.OrderItem
            {
                PrintingEditions = pe,
                Order = order,
                Amount = orderItem.Amount,
                Count = orderItem.Count,
                Currency = (DAL.Enums.PrintingEdition.Currency)orderItem.Currency,
                Id = orderItem.Id,
                CreationData = DateTime.Now,
                IsRemoved = false
            };

            await _orderItemRepository.Post(outOrderItem);
            return true;
        }

        public async Task<bool> DeleteOrderItem(int? id)
        {
            var entity = await _orderItemRepository.Get(id.Value);

            if (entity == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var orderItem = new DAL.Entities.OrderItem
            {
                Id = entity.Id,
                Amount = entity.Amount,
                Count = entity.Count,
                Currency = entity.Currency,
                Order = entity.Order,
                PrintingEditions = entity.PrintingEditions,
                CreationData = entity.CreationData,
                IsRemoved = true
            };
            _orderItemRepository.Put(id.Value, orderItem);
            return true;
        }

        public async Task<bool> OrderItemExists(int? id)
        {
            return await _orderItemRepository.Exists(id.Value);
        }
    }
}