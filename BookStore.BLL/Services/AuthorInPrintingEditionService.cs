﻿using AutoMapper;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;
using BookStore.DAL;
using BookStore.DAL.Repositories.Interfaces;
using BookStore.DAL.Repositories.EFRepositories;

namespace BookStore.BLL.Services
{
    public class AuthorInPrintingEditionService : IAuhorInPrintingEditionService
    {
        private IAuthorInPrintingEdition _authorInPrintingEditionRepository;
        private readonly StoreContext _storeContext;

        public AuthorInPrintingEditionService(StoreContext storeContext)
        {
            _authorInPrintingEditionRepository = new AuthorInPrintingEditionRepository(storeContext);
            _storeContext = storeContext;
        }

        public async Task<IEnumerable<AuthorInPrintingEdition>> GetAuthorInPrintingEdition(string name)
        {
            List<AuthorInPrintingEdition> outCollection = new List<AuthorInPrintingEdition>();

            var list = await _authorInPrintingEditionRepository.Get(name);

            #region Validation
            if (list == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }
            #endregion

            foreach(DAL.Entities.AuthorInPrintingEdition d in list)
            {
                var authorMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.Author, Author>()).CreateMapper();
                var dalAuthor = authorMapper.Map<DAL.Entities.Author, Author>(d.Author);

                var printingEditionMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.PrintingEdition, PrintingEdition>()).CreateMapper();
                var dalPrintingEdition = printingEditionMapper.Map<DAL.Entities.PrintingEdition, PrintingEdition>(d.PrintingEdition);

                var outAuthor = new AuthorInPrintingEdition
                {
                    Author = dalAuthor,
                    PrintingEdition = dalPrintingEdition,
                    Id = d.Id,
                    CreationData = d.CreationData,
                    IsRemoved = d.IsRemoved
                };

                outCollection.Add(outAuthor);
            }
            return outCollection;
        }

        public async Task<bool> PutAuthorInPrintingEdition(int? id, AuthorInPrintingEdition authorInPrintingEdition)
        {
            #region Validation
            if (authorInPrintingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            if(!(await AuthorInPrintingEditionExists(id)))
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var author = await _storeContext.Authors.FindAsync(authorInPrintingEdition.Author.Id);
            if (author == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var printingEdition = await _storeContext.PrintingEditions.FindAsync(authorInPrintingEdition.PrintingEdition.Id);
            if (printingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var dalAuthorInPrintingEdition = new DAL.Entities.AuthorInPrintingEdition
            {
                Author = author,
                PrintingEdition = printingEdition
            };

            dalAuthorInPrintingEdition.CreationData = authorInPrintingEdition.CreationData;
            dalAuthorInPrintingEdition.IsRemoved = authorInPrintingEdition.IsRemoved;

            _authorInPrintingEditionRepository.Put(id.Value, dalAuthorInPrintingEdition);
            return true;
        }
        
        public async Task<bool> PostAuthorInPrintingEdition(AuthorInPrintingEdition authorInPrintingEdition)
        {
            #region Validation
            if (authorInPrintingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_BAD_INPUT_DATA, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.BadRequest });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var author = await _storeContext.Authors.FindAsync(authorInPrintingEdition.Author.Id);
            if(author == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }

            var printingEdition = await _storeContext.PrintingEditions.FindAsync(authorInPrintingEdition.PrintingEdition.Id);
            if (printingEdition == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_MODEL_NOT_FOUND, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return false;
            }
            #endregion

            var dalAuthorInPrintingEdition = new DAL.Entities.AuthorInPrintingEdition
            {
                Author = author,
                PrintingEdition = printingEdition
            };

            dalAuthorInPrintingEdition.CreationData = DateTime.Now;
            dalAuthorInPrintingEdition.IsRemoved = false;

            await _authorInPrintingEditionRepository.Post(dalAuthorInPrintingEdition);
            return true;
        }

        public async Task<bool> AuthorInPrintingEditionExists(int? id)
        {
            return await _authorInPrintingEditionRepository.Exists(id.Value);
        }
    }
}