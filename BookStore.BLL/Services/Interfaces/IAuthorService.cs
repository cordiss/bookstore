﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IAuthorService
    {
        (IEnumerable<Author>, int, int) GetAuthors(string authorName, int page = 1);
        Author GetAuthor(int? id);
        Task<bool> PutAuthor(int? id, Author author);
        Task<bool> PostAuthor(Author author);
        bool DeleteAuthor(int? id);
        Task<bool> AuthorExists(int? id);
    }
}