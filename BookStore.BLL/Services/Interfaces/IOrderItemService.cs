﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IOrderItemService
    {
        IEnumerable<OrderItem> GetOrderItems();
        Task<OrderItem> GetOrderItem(int? id);
        Task<bool> PutOrderItem(int? id, OrderItem orderItem);
        Task<bool> PostOrderItem(OrderItem orderItem);
        Task<bool> DeleteOrderItem(int? id);
        Task<bool> OrderItemExists(int? id);
    }
}