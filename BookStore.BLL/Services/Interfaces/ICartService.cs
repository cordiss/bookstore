﻿using System;
using System.Threading.Tasks;
using BookStore.BLL.Models;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BLL.Services.Interfaces
{
    public interface ICartService
    {
        Task<int> CreateOrder(string username);
        Task<bool> AddOrderItem(int printingEditionId, int orderId, int count);
        Task<bool> ConfirmOrder(int orderId, string username);
    }
}