﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;
using System.Text;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IPrintingEditionService
    {
        (IEnumerable<PrintingEdition>, int, int) GetPrintingEditions(string title, string author, double minPrice = 0.0, double maxPrice = 99999.99, Enums.PrintingEdition.SortingMethod method = Enums.PrintingEdition.SortingMethod.CreationDataAsc, string type = "any", int page = 1);
        PrintingEdition GetPrintingEdition(int? id);
        Task<bool> PutPrintingEdition(int? id, PrintingEdition PrintingEdition);
        Task<int> PostPrintingEdition(PrintingEdition PrintingEdition);
        bool DeletePrintingEdition(int id);
        Task<bool> PrintingEditionExists(int? id);
    }
}