﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IOrderService
    {
        (IEnumerable<Order>, IEnumerable<OrderItem>, int, int) GetOrders(string userId, int page = 1, Enums.Order.Status status = Enums.Order.Status.Any, Enums.Order.Sorting sorting = Enums.Order.Sorting.CreationDataAsc);
        Task<(Order, IEnumerable<OrderItem>)> GetOrder(int? id, string userName = null);
        Task<bool> PutOrder(int? id, Order order);
        Task<int> PostOrder(Order order);
        Task<bool> DeleteOrder(int? id);
        Task<bool> OrderExists(int? id);
    }
}