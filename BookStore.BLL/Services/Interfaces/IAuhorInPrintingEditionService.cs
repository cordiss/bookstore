﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IAuhorInPrintingEditionService
    {
        Task<IEnumerable<AuthorInPrintingEdition>> GetAuthorInPrintingEdition(string name);
        Task<bool> PutAuthorInPrintingEdition(int? id, AuthorInPrintingEdition author);
        Task<bool> PostAuthorInPrintingEdition(AuthorInPrintingEdition authorInPrintingEdition);
    }
}