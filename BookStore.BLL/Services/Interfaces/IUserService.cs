﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;
using Microsoft.AspNetCore.Identity;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IUserService
    {
        
        Task<IdentityResult> ConfirmEmailAddress(string userId, string code);
        Task<IdentityResult> ConfirmResetPassword(string userId, string code, string newPassword);

        Task<IList<string>> UserRole(User user);
        (IEnumerable<User>, int, int) GetUsers(string firstName, string lastName, string email, bool isActive = true, int page = 1);

        Task<string> Create(User model, string password);
        Task<string> GenerateEmailToken(string id);
        Task<string> GetUserId(string userName);

        Task<(string, string)> GenerateResetPasswordToken(string email);        

        Task<User> GetUser(string id);
        Task<User> GetUserByName(string userName);
        Task<User> AuthenticateUser(string Email, string Password);
        
        Task SignOff();

        Task<bool> Edit(string id, User model);
        Task<bool> Delete(User model);
        Task<bool> BlockUser(string id, string username);
        Task<bool> UserInRole(User user, Enums.User.UserRole role);
        Task<bool> SignInUser(User user);
    }
}