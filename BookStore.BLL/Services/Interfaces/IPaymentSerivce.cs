﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.BLL.Services.Interfaces
{
    public interface IPaymentSerivce
    {
        (IEnumerable<Payment>, int, int) GetPayments(int page = 1);
        Payment GetPayment(int? id);
        Task<bool> PutPayment(int? id, Payment payment);
        Task<int> PostPayment(Payment payment);
        bool DeletePayment(int? id);
        Task<bool> PaymentExists(int? id);
    }
}