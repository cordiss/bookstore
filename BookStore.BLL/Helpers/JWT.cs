﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using BookStore.BLL.Services.Interfaces;
using BookStore.BLL.Helpers.Interfaces;
using BookStore.DAL;

namespace BookStore.BLL.Helpers
{
    public class JWT : IJwt
    {
        private readonly StoreContext _storeContext;
        private readonly IUserService _userService;
        public JWT(StoreContext storeContext)
        {
            _storeContext = storeContext;
        }

        public JWT(StoreContext storeContext, IUserService service)
            : this(storeContext)
        {
            _userService = service;
        }


        public object CreateTokens(IEnumerable<Claim> accesClaims, IEnumerable<Claim> refreshClaims, string username)
        {
            DateTime expiresData;
 
            AccesTokenGeneration accesGen = new AccesTokenGeneration();

            var newJwtToken = accesGen.GenerateAccesToken(accesClaims, out expiresData);
            var newRefreshToken = accesGen.GenerateAccesToken(refreshClaims, out expiresData);

            SaveRefreshToken(username, newRefreshToken);

            return new
            {
                token = newJwtToken,
                refreshToken = newRefreshToken,
                expires = expiresData
            };
        }

        public async Task<object> Refresh(string refreshToken)
        {
            DateTime expiresData;

            ExchangeToken exToken = new ExchangeToken();
            AccesTokenGeneration accesGen = new AccesTokenGeneration();

            var principal = exToken.GetPrincipalFromExpiredToken(refreshToken);

            if(principal == null)
            {
                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_INVALID_REFRESH, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);

                return null;
            }

            var username = principal.Identity.Name;

            var user = await _storeContext.Users.AsNoTracking().FirstOrDefaultAsync(e => e.UserName == username);

            string role = await GetUserRole(user);

            var savedRefreshToken = GetRefreshToken(user); //retrieve the refresh token from a data store

            if ((savedRefreshToken != refreshToken))
            {
                await _userService.SignOff();

                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_INVALID_REFRESH, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }

            
            if(user.RefreshExpires.CompareTo(DateTime.Now) < 0)
            {
                await _userService.SignOff();

                if (ErrorsModel.Errors.Count > Constants.MAX_ERRORS) ErrorsModel.Errors.Clear();

                ErrorsModel.Errors.Add(new ErrorsModel { Message = Constants.ErrorsConstants.ERROR_ACCES_INVALID_REFRESH, ErrorDate = DateTime.Now, ErrorPlace = this.ToString(), IsNew = true, StatusCode = HttpStatusCode.NotFound });

                ErrorsModel.Errors.OrderBy(e => e.ErrorDate);
                return null;
            }

            var accesClaims = new[]
            {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, role),
                    new Claim(ClaimTypes.Name, user.UserName.ToString())
            };

            var refreshClaims = new[]
            {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Name, user.UserName.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var newJwtToken = accesGen.GenerateAccesToken(accesClaims, out expiresData);
            var newRefreshToken = accesGen.GenerateAccesToken(refreshClaims, out expiresData);

            user.RefreshExpires = DateTime.Now.AddDays(60);
            user.RefreshToken = newRefreshToken;

            SaveRefreshToken(username, newRefreshToken);

            return new
            {
                token = newJwtToken,
                refreshToken = newRefreshToken,
                expires = expiresData
            };
        }

        private void SaveRefreshToken(string username, string refreshToken)
        {
            _storeContext.Database.ExecuteSqlRaw($"UPDATE dbo.AspNetUsers SET RefreshToken = '{refreshToken}', RefreshExpires = '{DateTime.Now.AddDays(60).ToString("s")}' WHERE UserName = '{username}'");
            _storeContext.SaveChanges();
        }
        private string GetRefreshToken(DAL.Entities.User user)
        {
            return user.RefreshToken;
        }

        private async Task<string> GetUserRole(DAL.Entities.User user)
        {
            var userMapper = new MapperConfiguration(cfg => cfg.CreateMap<DAL.Entities.User, Models.User>()).CreateMapper();
            var bllUser = userMapper.Map<DAL.Entities.User, Models.User>(user);

            IList<string> roles = await _userService.UserRole(bllUser);

            return roles[0];
        }
    }
}