﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace BookStore.BLL.Helpers
{
    public class AuthOptions
    {
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Constants.KEY));
        }
    }
}