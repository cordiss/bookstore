﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.BLL.Helpers.Interfaces
{
    public interface IJwt
    {
        object CreateTokens(IEnumerable<Claim> accesClaims, IEnumerable<Claim> refreshClaims, string username);
        Task<object> Refresh(string refreshToken);
    }
}