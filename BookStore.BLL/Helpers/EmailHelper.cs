﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace BookStore.BLL.Helpers
{
    public class EmailHelper
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(Constants.STORE_EMAIL_NAME, Constants.STORE_EMAIL));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(Constants.STORE_EMAIL_HOST, Constants.STORE_EMAIL_PORT, false);
                await client.AuthenticateAsync(Constants.STORE_EMAIL, Constants.STORE_EMAIL_PASSWORD);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}