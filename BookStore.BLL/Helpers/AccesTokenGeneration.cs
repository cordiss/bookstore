﻿using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace BookStore.BLL.Helpers
{
    public class AccesTokenGeneration
    {
        public string GenerateAccesToken(IEnumerable<Claim> claims, out DateTime expiresData)
        {
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: Constants.ISSUER,
                    audience: Constants.AUDIENCE,
                    notBefore: now,
                    claims: claims,
                    expires: now.Add(TimeSpan.FromMinutes(Constants.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            expiresData = now.Add(TimeSpan.FromMinutes(Constants.LIFETIME));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}