﻿namespace BookStore.Models
{
    public class UserAccountModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}