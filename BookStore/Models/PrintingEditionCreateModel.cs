﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.BLL.Models;

namespace BookStore.Models
{
    public class PrintingEditionCreateModel
    {
        public PrintingEdition PrintingEdition { get; set; }
        public Author Author { get; set; }
    }
}