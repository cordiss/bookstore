﻿using System.Net;
using Microsoft.Extensions.Logging;

namespace BookStore.ErrorHandler
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TController"></typeparam>
    public static class Handler<TController> 
        where TController : class
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static HttpStatusCode ReturnError(ILogger<TController> logger)
        {
            var error = ErrorsModel.Errors.FindLast(e => e.IsNew == true);

            logger.LogError($"{error.ErrorDate}: {error?.Message} | Place: {error?.ErrorPlace}");

            error.IsNew = false;

            return error.StatusCode;
        }
    }
}
