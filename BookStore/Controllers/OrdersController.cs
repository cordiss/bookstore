﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using BookStore.ErrorHandler;
using BookStore.BLL;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private IOrderService _orderService;
        private readonly ILogger<OrdersController> _logger;
        private readonly IUserService _userService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="userService"></param>
        /// <param name="logger"></param>
        public OrdersController(IOrderService service, IUserService userService, ILogger<OrdersController> logger)
        {
            _orderService = service;
            _logger = logger;
            _userService = userService;
        }

        /// <summary>
        /// Getting page of Orders with some filters
        /// For Client only his orders
        /// For Admin all orders
        /// </summary>
        /// <param name="userId">ID of user that made order</param>
        /// <param name="page">Page of Orders collection</param>
        /// <param name="status">Status of Orders</param>
        /// <param name="sorting">Sorting method of Orders collection</param>
        /// <returns>IEnumerable of Orders</returns>
        [HttpGet]
        public async Task<ActionResult> GetOrders(string userId, int page = 1, Enums.Order.Status status = Enums.Order.Status.Any, Enums.Order.Sorting sorting = Enums.Order.Sorting.CreationDataAsc)
        {
            if (User.Identity.IsAuthenticated)
            {
                (IEnumerable<Order>, IEnumerable<OrderItem>, int, int) result = (null, null, 0, 0);

                if(User.HasClaim(ClaimTypes.Role, "client"))
                {
                    string userID = await _userService.GetUserId(User.Identity.Name);
                    result = _orderService.GetOrders(userID, page, status, sorting);
                }
                if(User.HasClaim(ClaimTypes.Role, "admin"))
                {
                    result = _orderService.GetOrders(userId, page, status, sorting);
                }

                var response = new
                {
                    Orders = result.Item1,
                    OrderItems = result.Item2,
                    TotalPages = result.Item3,
                    CurrentPage = result.Item4
                };

                _logger.LogInformation($"Getting all orders...");
                return Ok(response);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Getting concrete order by it ID
        /// </summary>
        /// <param name="id">ID of Order</param>
        /// <returns>Order model</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                (Order, IEnumerable<OrderItem>) result = (null, null);

                _logger.LogInformation($"Getting order ({id})...");

                if (User.HasClaim(ClaimTypes.Role, "client"))
                {
                    result = await _orderService.GetOrder(id, User.Identity.Name);                    
                }
                else
                {
                    result = await _orderService.GetOrder(id);
                }

                if (result.Item1 == null && result.Item2 == null)
                {
                    var statusCode = Handler<OrdersController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }

                var response = new
                {
                    Order = result.Item1,
                    OrderItems = result.Item2
                };

                return Ok(response);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Updating concrete Order
        /// </summary>
        /// <param name="id">ID of Order</param>
        /// <param name="order">New model of Order</param>
        /// <returns>OK if all went well</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, Order order)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Updating order ({id})...");
                bool succes = await _orderService.PutOrder(id, order);
                if (!succes)
                {
                    var statusCode = Handler<OrdersController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Marks an Order as removed.
        /// </summary>
        /// <param name="id">ID of Order</param>
        /// <returns>OK if all went well</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteOrder(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Deleting order...");

                var succes = await _orderService.DeleteOrder(id);
                if (!succes)
                {
                    var statusCode = Handler<OrdersController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }
    }
}