﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookStore.ErrorHandler;
using BookStore.BLL.Services.Interfaces;
using BookStore.BLL;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private IUserService _userService;
        private readonly ILogger<RolesController> _logger;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public RolesController(IUserService service, ILogger<RolesController> logger)
        {
            _userService = service;
            _logger = logger;
        }

        /// <summary>
        /// Gets all users roles
        /// </summary>
        /// <param name="id">ID of User</param>
        /// <returns>IEnumerable with all roles of user</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<string>>> GetUserRoles(string id)
        {
            _logger.Log(LogLevel.Information, $"Getting user ({id}) roles...");

            var user = await _userService.GetUser(id);

            if(user == null)
            {
                var statusCode = Handler<RolesController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }

            return Ok(await _userService.UserRole(user));
        }

        /// <summary>
        /// Checks if user have a concrete role
        /// </summary>
        /// <param name="id">ID of user</param>
        /// <param name="role">Concrete role</param>
        /// <returns>true when user have this role, false when doesn't</returns>
        [HttpGet]
        public async Task<ActionResult<bool>> GetUserRole(string id, Enums.User.UserRole role)
        {
            _logger.Log(LogLevel.Information, $"Getting user ({id}) role...");

            var user = await _userService.GetUser(id);
            if (user == null)
            {
                var statusCode = Handler<RolesController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }

            return Ok(await _userService.UserInRole(user, role));
        }       
    }
}