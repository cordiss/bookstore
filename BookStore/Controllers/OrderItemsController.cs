﻿using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrderItemsController : ControllerBase
    {
        private IOrderItemService _orderItemService;
        private readonly ILogger<OrderItemsController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public OrderItemsController(IOrderItemService service, ILogger<OrderItemsController> logger)
        {
            _orderItemService = service;
            _logger = logger;
        }

        /// <summary>
        /// Updates concrete Order Item
        /// </summary>
        /// <param name="id">ID of Order Item</param>
        /// <param name="orderItem">Model of new Order Item</param>
        /// <returns>OK if all went well</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderItem(int id, OrderItem orderItem)
        {
            if (User.Identity.IsAuthenticated)
            {
                _logger.LogInformation($"Updating order item ({id})");
                var succes = await _orderItemService.PutOrderItem(id, orderItem);
                if (!succes)
                {
                    var statusCode = Handler<OrderItemsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Deletes concrete Order Item
        /// </summary>
        /// <param name="id">ID of Order Item</param>
        /// <returns>OK if all went well</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteOrderItem(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Deleting order item...");

                var entity = await _orderItemService.GetOrderItem(id);
                if (entity == null)
                {
                    var statusCode = Handler<OrderItemsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else
                {
                    entity.IsRemoved = true;

                    var succes = await _orderItemService.PutOrderItem(id, entity);
                    if (!succes)
                    {
                        var statusCode = Handler<OrderItemsController>.ReturnError(_logger);

                        return StatusCode((int)statusCode);
                    }
                    else return Ok();
                }
            }
            else return StatusCode(403);
        }
    }
}