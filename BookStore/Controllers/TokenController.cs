﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using BookStore.ErrorHandler;
using BookStore.BLL.Helpers;
using BookStore.BLL.Services.Interfaces;
using BookStore.BLL.Helpers.Interfaces;
using BookStore.DAL;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IUserService _userService;
        private IJwt _jwt;
        private readonly ILogger<TokenController> _logger;
        private readonly StoreContext _storeContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="jwt"></param>
        /// <param name="storeContext"></param>
        /// <param name="logger"></param>
        public TokenController(IUserService service, IJwt jwt, StoreContext storeContext, ILogger<TokenController> logger)
        {
            _storeContext = storeContext;
            _userService = service;
            _jwt = jwt;
            _logger = logger;
        }

        /// <summary>
        /// Refreshes a pair of tokens, and sends to user a new pair.
        /// </summary>
        /// <param name="refreshtoken">JSON object that contains Refresh Token</param>
        /// <returns>JSON object with Acces, Refresh Tokens and DateTime when Acces Token expires</returns>
        [HttpPost]
        public async Task<ActionResult<object>> Refresh(object refreshtoken)
        {
            _jwt = new JWT(_storeContext, _userService);
            Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(refreshtoken.ToString(), new JsonSerializerSettings { Formatting = Formatting.Indented });

            _logger.Log(LogLevel.Information, "User trying to refresg his tokens...");

            object response = await _jwt.Refresh(result["refreshtoken"]);

            if (response == null)
            {
                var statusCode = Handler<TokenController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }
            else
            {
                HttpContext.Response.Cookies.Append("Token", response.ToString());

                return Ok(response);

            }
        }
    }
}