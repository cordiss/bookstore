﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Claims;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Helpers;
using BookStore.BLL.Services.Interfaces;
using BookStore.BLL.Helpers.Interfaces;
using BookStore.Models;

namespace BookStore.Controllers
{
    /// <summary>
    /// Class of Account Controller:
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IUserService _userService;
        private IJwt _jwt;
        private readonly ILogger<AccountController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="jwt"></param>
        /// <param name="logger"></param>
        public AccountController(IUserService service, IJwt jwt, ILogger<AccountController> logger)
        {
            _jwt = jwt;
            _userService = service;
            _logger = logger;
        }
        /// <summary>
        /// Authenticate user with his email and password. 
        /// </summary>
        /// <param name="user">User Account Model that contains (user.Email, user.Password).</param>
        /// <returns>JSON object with Acces, Refresh Tokens with DateTime when Acces Token expires.</returns>
        [HttpPost]
        public async Task<ActionResult> AuthenticateUser(UserAccountModel user)
        {
            var usr = await _userService.GetUserByName(user.Email);

            if (usr.IsActive == true)
            {
                _logger.LogInformation($"Try to authenticate user ({user.Email} {user.Password})...");

                User authUser = await _userService.AuthenticateUser(user.Email, user.Password);

                if (authUser == null)
                {
                    await SignOffUser();

                    var statusCode = Handler<AccountController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else
                {
                    IList<string> roles = await _userService.UserRole(authUser);

                    _logger.LogError(roles[0]);

                    authUser.Role = roles[0];

                    object response = Token(authUser);

                    _logger.LogInformation($"User ({user.Email}) authenticated");

                    return Ok(response);

                }
            }
            else
            {
                await SignOffUser();
                return StatusCode(403, "Acces denied. Your account was blocked by administrator.");
            }
        }

        /// <summary>
        /// Sign user that authenticated off.
        /// </summary>
        /// <returns>Status Code OK if all went as it must.</returns>
        [HttpDelete]
        public async Task<ActionResult> SignOffUser()
        {
            _logger.LogInformation($"Sign off...");
            await _userService.SignOff();

            HttpContext.Response.Cookies.Delete("Token");
            return Ok();
        }

        /// <summary>
        /// Method that confirms email of user, that recieve a reference on email after signing up.
        /// </summary>
        /// <param name="userId">ID of user</param>
        /// <param name="code">Unical code generated through the procedure Sign Up</param>
        /// <returns>OK if email is confirmed</returns>
        /// <returns>Unauthorized if something went wrong</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null) return StatusCode(404);

            code = System.Web.HttpUtility.UrlDecode(code);

            var result = await _userService.ConfirmEmailAddress(userId, code);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return StatusCode(401);
            }
        }
        /// <summary>
        /// Reseting password of user and set a new. Sends email message to user with a link to reset pass.
        /// </summary>
        /// <param name="email">Emaill of user</param>
        /// <returns>OK if link was sended.</returns>
        [HttpPut]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(object email)
        {
            Dictionary<string, string> Email = JsonConvert.DeserializeObject<Dictionary<string, string>>(email.ToString(), new JsonSerializerSettings { Formatting = Formatting.Indented });

            (string, string) result = await _userService.GenerateResetPasswordToken(Email["email"]);
            if (result.Item1 == null && result.Item2 == null)
            {
                var statusCode = Handler<AccountController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }
            else
            {
                var callbackUrl = Url.Action("ConfirmResetPassword", "Account", new { userId = result.Item1, code = result.Item2 }, protocol: HttpContext.Request.Scheme);

                EmailHelper emailHelper = new EmailHelper();
                await emailHelper.SendEmailAsync(Email["email"], "Reset Password",
                    $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>{callbackUrl}</a>");

                return Ok();
            }
        }

        /// <summary>
        /// Confirming reseting password for user that have a reference on his email.
        /// </summary>
        /// <param name="value">JSON object that contains User Id, Reset Password Token, New Password</param>
        /// <returns>OK if password was update</returns>
        [Route("[controller]/[action]")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmResetPassword(object value)
        {
            Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(value.ToString(), new JsonSerializerSettings { Formatting = Formatting.Indented });

            var userId = result["userId"];
            var code = result["code"];
            var newPassword = result["newPassword"];

            code = System.Web.HttpUtility.UrlDecode(code);

            await _userService.ConfirmResetPassword(userId, code, newPassword);
            return Ok();
        }

        private object Token(User user)
        {
            var accesClaims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.Role.ToString()),
                new Claim(ClaimTypes.Name, user.UserName.ToString()),
            };

            var refreshClaims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Name, user.UserName.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            object response = _jwt.CreateTokens(accesClaims, refreshClaims, user.UserName);

            HttpContext.Response.Cookies.Append("Token", response.ToString());
            return response;
        }        
    }
}