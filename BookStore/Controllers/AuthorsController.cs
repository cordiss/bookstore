﻿using System.Security.Claims;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly ILogger<AuthorsController> _logger;
        private IAuthorService _authorService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorService"></param>
        /// <param name="logger"></param>
        public AuthorsController(IAuthorService authorService, ILogger<AuthorsController> logger)
        {
            _authorService = authorService;
            _logger = logger;
        }

        /// <summary>
        /// Getting all authors, with filter (Author Name)
        /// </summary>
        /// <param name="authorName">Author Name</param>
        /// <param name="page">Page of collection (Default value - 1)</param>
        /// <returns>IEnumerable collection of Authors</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Author>> GetAuthors(string authorName, int page = 1)
        {
            if (User.Identity.IsAuthenticated)
            {
                _logger.Log(LogLevel.Information, "Getting all authors...");

                var result = _authorService.GetAuthors(authorName, page);

                var response = new
                {
                    Authors = result.Item1,
                    TotalPages = result.Item2,
                    CurrentPage = result.Item3
                };

                return Ok(response);
            }
            else return StatusCode(401);
        }

        /// <summary>
        /// Get concrete author by his ID
        /// </summary>
        /// <param name="id">ID of author</param>
        /// <returns>Author model</returns>
        [HttpGet("{id}")]
        public ActionResult<Author> GetAuthor(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.Log(LogLevel.Information, $"Getting author by Id({id})...");
                var author = _authorService.GetAuthor(id);

                if(author == null)
                {
                    var statusCode = Handler<AuthorsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok(author);
            }
            else
            {
                return StatusCode(403);
            }
        }

        /// <summary>
        /// Update concrete author by his ID
        /// </summary>
        /// <param name="id">ID of author</param>
        /// <param name="author">New Author model</param>
        /// <returns>OK if all went well</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author author)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.Log(LogLevel.Information, $"Updating author by Id({id})...");
                var succes = await _authorService.PutAuthor(id, author);
                if (!succes)
                {
                    var statusCode = Handler<AuthorsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Create a new author for admin
        /// </summary>
        /// <param name="author">Model of new Author</param>
        /// <returns>Status Code 201 if all went well</returns>
        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor(Author author)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.Log(LogLevel.Information, $"Creating new author...");
                var succes = await _authorService.PostAuthor(author);
                if (!succes)
                {
                    var statusCode = Handler<AuthorsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return StatusCode(201);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Marks concrete author as removed.
        /// </summary>
        /// <param name="id">ID of author</param>
        /// <returns>OK if all went well</returns>
        [HttpDelete("{id}")]
        public ActionResult DeleteAuthor(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.Log(LogLevel.Information, $"Deleting author... ({id})");

                var succes = _authorService.DeleteAuthor(id);
                if (!succes)
                {
                    var statusCode = Handler<AuthorsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }
    }
}