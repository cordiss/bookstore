﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Helpers;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private readonly ILogger<UserController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public UserController(IUserService service, ILogger<UserController> logger)
        {
            _userService = service;
            _logger = logger;
        }

        /// <summary>
        /// Gets all users with some filters
        /// </summary>
        /// <param name="firstName">First Name of user</param>
        /// <param name="lastName">Last Name of user</param>
        /// <param name="email">Email of user</param>
        /// <param name="isActive">Is user Active filter</param>
        /// <param name="page">Page of users collection</param>
        /// <returns>IEnumerable of users</returns>
        [HttpGet]
        public ActionResult<IEnumerable<User>> GetUsers(string firstName, string lastName, string email, bool isActive = true, int page = 1)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                var result = _userService.GetUsers(firstName, lastName, email, isActive, page);

                var response = new
                {
                    Users = result.Item1,
                    TotalPages = result.Item2,
                    CurrentPage = result.Item3
                };
                return Ok(response);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Gets concrete user by it ID
        /// </summary>
        /// <param name="id">ID of user</param>
        /// <returns>User model</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(string id)
        {
            _logger.LogInformation($"Getting user with id ({id})...");

            var user = await _userService.GetUser(id);

            if(user == null)
            {
                var statusCode = Handler<UserController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }
            else return Ok(user);
        }

        /// <summary>
        /// Mark concrete user as unactive. Bloks authentication to this user.
        /// Route: .../api/User/User/BlockUser?id=userId
        /// </summary>
        /// <param name="id">ID of user</param>
        /// <returns>OK if all went well</returns>
        [Route("[controller]/[action]")]
        [HttpGet]
        public async Task<ActionResult> BlockUser(string id)
        {

            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogError("Trying to block user");

                string username = User.Identity.Name;

                var succes = await _userService.BlockUser(id, username);
                if (!succes)
                {
                    var statusCode = Handler<UserController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Updates a concrete user by his ID
        /// </summary>
        /// <param name="id">ID of User</param>
        /// <param name="user">A new User model</param>
        /// <returns>OK if all went well</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(string id, User user)
        {
            if (User.Identity.IsAuthenticated)
            {
                _logger.LogInformation($"Updating user ({id})...");

                user.Id = id;

                var succes = await _userService.Edit(id, user);
                if (!succes)
                {
                    var statusCode = Handler<UserController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Creates a new User throuh signing up. Generates EmailConfirmToken and sends it on email.
        /// </summary>
        /// <param name="user">Model of new user</param>
        /// <returns>User model</returns>
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _logger.LogInformation($"Creating user ({user.Id}: {user.FirstName} {user.LastName} {user.Password})");
            var result = await _userService.Create(user, user.Password);

            if (result == null)
            {
                var statusCode = Handler<UserController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }
            else
            {
                user.Id = result;

                var code = await _userService.GenerateEmailToken(result);
                await ConfirmEmail(user, code);

                return StatusCode(201);
            }
        }

        private async Task ConfirmEmail(User user, string code)
        {
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
            EmailHelper email = new EmailHelper();
            await email.SendEmailAsync(user.Email, "Book Store - Confirm Sign Up", $"Confirm your email address: <a href='{callbackUrl}'>Confirm</a>");
        }

        /// <summary>
        /// Deletes a concrete user by his ID
        /// </summary>
        /// <param name="id">ID of user</param>
        /// <returns>OK if all went well</returns>

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(string id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                var user = await _userService.GetUser(id);

                if (user == null)
                {
                    var statusCode = Handler<UserController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }

                _logger.LogInformation($"Deleting user ({user.Id})...");

                var succes = await _userService.Delete(user);
                if (!succes)
                {
                    var statusCode = Handler<UserController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }        
    }
}