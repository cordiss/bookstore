﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorInPrintingEditionsController : ControllerBase
    {
        private IAuhorInPrintingEditionService _authorService;
        private readonly ILogger<AuthorInPrintingEditionsController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public AuthorInPrintingEditionsController(IAuhorInPrintingEditionService service, ILogger<AuthorInPrintingEditionsController> logger)
        {
            _authorService = service;
            _logger = logger;
        }

        /// <summary>
        /// Getting printing editions of author
        /// </summary>
        /// <param name="name">Param name: Name of author</param>
        /// <returns>IEnumerable collection of Authors and theese Printing Editions</returns>
        [HttpGet]
        public async Task<ActionResult<AuthorInPrintingEdition>> GetAuthorInPrintingEdition(string name)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Getting author in printing edition ({name})");
                var author = await _authorService.GetAuthorInPrintingEdition(name);
                if (author == null)
                {
                    var statusCode = Handler<AuthorInPrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok(author);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Update Author and his Printing Editions
        /// </summary>
        /// <param name="id">ID of author</param>
        /// <param name="author">New model of author</param>
        /// <returns>OK if all went well</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthorInPrintingEdition(int id, AuthorInPrintingEdition author)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Updating author in printing edition ({id})");
                var succes = await _authorService.PutAuthorInPrintingEdition(id, author);
                if (!succes)
                {
                    var statusCode = Handler<AuthorInPrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }
    }
}