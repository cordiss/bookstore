﻿using System.Security.Claims;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using BookStore.Models;
using BookStore.ErrorHandler;
using BookStore.BLL;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PrintingEditionsController : ControllerBase
    {
        private IPrintingEditionService _printingEditionService;
        private ICartService _cartService;
        private readonly IAuhorInPrintingEditionService _auhorInPrintingEditionService;

        private readonly ILogger<PrintingEditionsController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="cartService"></param>
        /// <param name="auhorInPrintingEditionService"></param>
        /// <param name="logger"></param>
        public PrintingEditionsController(IPrintingEditionService service, ICartService cartService, IAuhorInPrintingEditionService auhorInPrintingEditionService, ILogger<PrintingEditionsController> logger)
        {
            _printingEditionService = service;
            _cartService = cartService;
            _auhorInPrintingEditionService = auhorInPrintingEditionService;
            _logger = logger;
        }

        /// <summary>
        /// Getting all printing editions with some filters
        /// </summary>
        /// <param name="title">Title of PE</param>
        /// <param name="author">Author of PE</param>
        /// <param name="minPrice">Minimal Price</param>
        /// <param name="maxPrice">Maximum Price</param>
        /// <param name="method">Sorting method</param>
        /// <param name="type">Type of PE</param>
        /// <param name="page">Page of PEs collection</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<PrintingEdition>> GetPrintingEditions(string title, string author, double minPrice = 0.0, double maxPrice = 99999.99, Enums.PrintingEdition.SortingMethod method = Enums.PrintingEdition.SortingMethod.CreationDataAsc, string type = "any", int page = 1)
        {
            _logger.LogInformation($"Getting all printing editions...({author})");

            var pageIndexModel = _printingEditionService.GetPrintingEditions(title, author, minPrice, maxPrice, method, type, page);

            var result = new
            {
                PrintingEditions = pageIndexModel.Item1,
                TotalPages = pageIndexModel.Item2,
                CurrentPage = pageIndexModel.Item3
            };

            return Ok(result);
        }

        /// <summary>
        /// Getting concrete Printing Edition by it ID
        /// </summary>
        /// <param name="id">ID of PE</param>
        /// <returns>Printing Edition model</returns>
        [HttpGet("{id}")]
        public ActionResult<PrintingEdition> GetPrintingEdition(int id)
        {
            _logger.LogInformation($"Getting printing edition ({id})...");
            var printingEdition = _printingEditionService.GetPrintingEdition(id);

            if (printingEdition == null)
            {
                var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                return StatusCode((int)statusCode);
            }
            else return Ok(printingEdition);
        }

        /// <summary>
        /// Updating a Printing Edition
        /// </summary>
        /// <param name="id">ID of PE</param>
        /// <param name="PrintingEdition">New model of PE</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrintingEdition(int id, PrintingEdition PrintingEdition)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Updating printing edition ({id})...");
                var succes = await _printingEditionService.PutPrintingEdition(id, PrintingEdition);
                if (!succes)
                {
                    var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Buy Action of Printing Edition. Makes a new order or adds a new order item to existing order.
        /// Route: ...api/PrintingEditions/PrintingEditions/BuyPrintingEdition
        /// </summary>
        /// <param name="id">ID of PE</param>
        /// <returns>OK if all went well</returns>
        [Route("[controller]/[action]")]
        [HttpPost]
        public async Task<ActionResult> BuyPrintingEdition(object id)
        {
            if (User.Identity.IsAuthenticated)
            {
                Dictionary<string, int> result = JsonConvert.DeserializeObject<Dictionary<string, int>>(id.ToString());

                string username = User.Identity.Name;

                int orderId = await _cartService.CreateOrder(username);

                if (orderId == -1)
                {
                    var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else
                {
                    var succes = await _cartService.AddOrderItem(result["printingEdition"], orderId, result["count"]);
                    if (!succes)
                    {
                        var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                        return StatusCode((int)statusCode);
                    }
                    else return Ok();
                }

            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Confirms an existing order and marks it as Closed.
        /// Route: ...api/PrintingEditions/PrintingEditions/ConfirmOrder?id=4
        /// </summary>
        /// <param name="id">ID of Order</param>
        /// <returns></returns>
        [Route("[controller]/[action]")]
        [HttpGet]
        public async Task<ActionResult> ConfirmOrder(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                _logger.LogError($"Confirm order {id}");

                string username = User.Identity.Name;
                var succes = await _cartService.ConfirmOrder(id, username);
                if (!succes)
                {
                    var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Creates a new Printing Edition
        /// </summary>
        /// <param name="model">Model of a new Printing Edition</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PrintingEdition>> PostPrintingEdition(PrintingEditionCreateModel model)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Creating printing edition...");
                int id = await _printingEditionService.PostPrintingEdition(model.PrintingEdition);

                var pe = _printingEditionService.GetPrintingEdition(id);

                if (pe == null)
                {
                    var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else
                {
                    var authorInPE = new AuthorInPrintingEdition
                    {
                        PrintingEdition = pe,
                        Author = model.Author
                    };

                    var succes = await _auhorInPrintingEditionService.PostAuthorInPrintingEdition(authorInPE);
                    if (!succes)
                    {
                        var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                        return StatusCode((int)statusCode);
                    }
                    else return StatusCode(201);
                }
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Mark concrete PE as removed
        /// </summary>
        /// <param name="id">ID of PE</param>
        /// <returns>OK if all went well</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePrintingEdition(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                var entity = _printingEditionService.GetPrintingEdition(id);

                entity.IsRemoved = true;

                _logger.LogInformation($"Deleting printing edition ({id})...");
                var succes = await _printingEditionService.PutPrintingEdition(id, entity);

                if (!succes)
                {
                    var statusCode = Handler<PrintingEditionsController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok();
            }
            else return StatusCode(403);
        }
    }
}