﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BookStore.ErrorHandler;
using BookStore.BLL.Models;
using BookStore.BLL.Services.Interfaces;

namespace BookStore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private IPaymentSerivce _paymentService;
        ILogger<PaymentController> _logger;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public PaymentController(IPaymentSerivce service, ILogger<PaymentController> logger)
        {
            _paymentService = service;
            _logger = logger;
        }

        /// <summary>
        /// Getting all payments
        /// </summary>
        /// <param name="page">Page of Payments collection</param>
        /// <returns>IEnumerable of payments</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Payment>> GetPayments(int page = 1)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Getting all payments...");

                var result = _paymentService.GetPayments(page);

                var response = new
                {
                    Payments = result.Item1,
                    TotalPages = result.Item2,
                    CurrentPage = result.Item3
                };

                return Ok(response);
            }
            else return StatusCode(403);
        }

        /// <summary>
        /// Getting concrete payment by it ID
        /// </summary>
        /// <param name="id">ID of payment</param>
        /// <returns>Payment model</returns>
        [HttpGet("{id}")]
        public ActionResult<Payment> GetPayment(int id)
        {
            if (User.Identity.IsAuthenticated && User.HasClaim(ClaimTypes.Role, "admin"))
            {
                _logger.LogInformation($"Getting payment ({id})...");
                var payment = _paymentService.GetPayment(id);

                if (payment == null)
                {
                    var statusCode = Handler<PaymentController>.ReturnError(_logger);

                    return StatusCode((int)statusCode);
                }
                else return Ok(payment);
            }
            else return StatusCode(403);
        }
    }
}